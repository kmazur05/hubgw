#!/bin/bash
#to invoke ./deploy.sh $ACTION $JOBNAME
#to invoke example ./deploy.sh build  whizzapiassets-tst-gw-build

    PROPERTY_FILE=../deployment/config.cfg
    JOBNAME=$2
    ACTION=$1
    #get property from input
    ENV=`echo $JOBNAME | cut -d"-" -f2`
    NODE=`echo $JOBNAME | cut -d"-" -f3`
    BRANCH=$ENV
     function getProperty {
       PROP_KEY=$1
       PROP_VALUE=`cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
       echo $PROP_VALUE
    }

    #get config property
    SOURCE_IS_HOSTNAME=$(getProperty "env.dev.hostname")
    SOURCE_IS_USER=$(getProperty "env.dev.$NODE.user")
    SOURCE_IS_PASS=$(getProperty "env.dev.$NODE.pass")
    SOURCE_IS_PORT=$(getProperty "env.dev.$NODE.port")
    ASSETS_FILE=$(getProperty "env.dev.$NODE.assets.filename")
    REPOSITORY="$(getProperty "repo.dir")"
    BUILD_DIR="$(getProperty "abe.home.dir")/bin/"
    URL="http://${SOURCE_IS_HOSTNAME}:$SOURCE_IS_PORT"
    DEP_DIR="$(getProperty "deployer.home.dir")/bin/"
    DEP_PROJECT=whizzapi_`echo $ENV`_`echo $ACTION | cut -d"_" -f2-3`

    function pushToRepository {
       REPOSITORY=$1
       JOBNAME=$2
       cd $REPOSITORY;
       ARE_CHANGES=`git status | egrep "modified|deleted|new file" | wc -l`
       if [ "$ARE_CHANGES" -ge 1 ]
       then
         git add -Af .  && echo "git commit..." &&  git commit -m "commited by jenkins job $JOBNAME" && git push origin $BRANCH
         echo 
         exit 0
       fi

    }
        #simply validate env input parameter
        if [ ${#ENV} -ne 3 ] && [ $ENV != 'master' ];
        then
          echo "Wrong env name in job name."; exit 1
        else echo "Environment parameter verified correctly."
        fi
        #prepare branch parameter
	if [ ${#ENV} -eq 3 ];
	then BRANCH="master_$ENV"
	fi

#set branch		
#	ssh wm@$SOURCE_IS_HOSTNAME "cd $REPOSITORY; git fetch && git reset --hard && git clean -fd && git checkout $ENV && git pull origin $ENV"	
        ssh wm@$SOURCE_IS_HOSTNAME "cd $REPOSITORY; git fetch && git checkout $BRANCH && git pull origin $BRANCH"
	echo "Branch was changed to: $BRANCH"

	echo "Executing action: $ACTION"
#check operation to execute
        if [ $ACTION = 'build' ]; then
           echo "Execute command ./build.sh -Dbuild.output.dir=$REPOSITORY/assets -Dapigateway.is.url=$URL -Dapigateway.is.username=$SOURCE_IS_USER -Dapigateway.is.password=****** -Dapigateway.assets.file=$ASSETS_FILE"
           ssh wm@$SOURCE_IS_HOSTNAME "cd $BUILD_DIR && ./build.sh -Dbuild.output.dir=$REPOSITORY/assets -Dapigateway.is.url=$URL -Dapigateway.is.username=$SOURCE_IS_USER -Dapigateway.is.password=$SOURCE_IS_PASS -Dapigateway.assets.file=$ASSETS_FILE"
        elif [ $ACTION = 'push_repository' ]; then
             ssh wm@$SOURCE_IS_HOSTNAME "`declare -f pushToRepository` && pushToRepository $REPOSITORY $JOBNAME"
        elif [ $ACTION = 'deploy_gw_packages' ] || [ $ACTION = 'deploy_gw_assets' ]; then
             ssh wm@$SOURCE_IS_HOSTNAME "cd $DEP_DIR && ./Deployer.sh --deploy -dc myDeployment -project $DEP_PROJECT -host $SOURCE_IS_HOSTNAME -port $SOURCE_IS_PORT -user $SOURCE_IS_USER -pwd $SOURCE_IS_PASS -force -reportFilePath $DEP_DIR"
        elif [ $ACTION = 'simulation' ]; then
             echo "/TODO for assets/ Executing action: $ACTION"
        else echo "Wrong ACTION parameter."
        fi

